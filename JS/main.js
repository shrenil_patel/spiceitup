////////////////////////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////////////////////////

screenWidth = 800;
screenHeight = 600;
time = 0;               //time increment

grav = 0.5;             //gravity
timefctr = 1.0;         //time factor

//up,down,left,right,jump,att,item
//how long since last key press of same key
//how long key has been held down
keys = [false,false,false,false,false,false,false,
0,0,0,0,0,0,0,
0,0,0,0,0,0,0];


////////////////////////////////////////////////////////////////////////////////
// Ajax sync script require
// sync aditional JS scripts, makes things modular
////////////////////////////////////////////////////////////////////////////////

function require(jsFilePath) {
    var js = document.createElement("script");

    js.type = "text/javascript";
    js.src = jsFilePath;

    document.body.appendChild(js);
}

require("JS/parent_blocks.js");
require("JS/player.js");

////////////////////////////////////////////////////////////////////////////////
// Event loop
////////////////////////////////////////////////////////////////////////////////

window.onload = function() {

  var c = document.getElementById('screen').getContext('2d');
  c.canvas.width = screenWidth;
  c.canvas.height = screenHeight;

	this.objs = [];
	this.objs.push(new player(32.0,32.0,32,32));
	this.objs.push(new display(0,320,320,32));

    this.objs.push(new polyBlock([0,0,32],[0,32,32]));

  //game.setup();
  setInterval(function() {

  	//clear screen
  	c.clearRect(0,0,screenWidth,screenHeight);

    for (var i = 0; i < this.objs.length; i++) {
    	this.objs[i].draw(c);
    	this.objs[i].update(this.objs, keys);

    }

  }, 1000 / 60);
};

function actionForEvent(e) {
  var key = e.which;
  if (key == 32) return 4; //'fire';
  if (key == 37) return 2; //'left';
  if (key == 38) return 0; //'up';
  if (key == 39) return 3; //'right';
  if (key == 40) return 1; //'down';
  return null;
}

window.onkeydown = function(e) {
  var action = actionForEvent(e);
  keys[action] = true;
};

window.onkeyup = function(e) {
  var action = actionForEvent(e);
  keys[action] = false;
};
