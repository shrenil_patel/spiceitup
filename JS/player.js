////////////////////////////////////////////////////////////////////////////////
// Player
////////////////////////////////////////////////////////////////////////////////


function player(x, y, width, height) {
    instance.call(this, width, height, x, y);
}

player.prototype.draw = function (c) {
    c.fillStyle = "yellow";
    c.fillRect(this.x,this.y,this.width,this.height);
}

player.prototype.update = function (b, keys) {

    if (!COLL_RxR_Place(this,b,0,1)){
        this.vsp += grav;
    }else{
        this.vsp = 0;
        if (keys[0]){
            this.vsp -= 9;
        }
    }

    if (keys[2] && !keys[3]){
        this.hsp = -3;
    }else if (keys[3] && !keys[2]){
        this.hsp = 3;
    }else if (!keys[2] && !keys[3]){
        this.hsp = 0;
    }

    simp_move(this,b);

}