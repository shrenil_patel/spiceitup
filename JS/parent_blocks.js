////////////////////////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// Collision Functions
////////////////////////////////////////////////////////////////////////////////

function COLL_RxR_Place(rect1, b, x, y){
    var rectX = {x: rect1.x+x, y: rect1.y+y, width: rect1.width, height: rect1.height};

    for (var i = 0; i < b.length; i++) {
        if (b[i].bound === "rect"){
            if (COLL_RxR(rectX, b[i]) && rect1 != b[i])
                return true;
        }
    }

    return false;
}

function COLL_RxR(rect1, rect2){
    return (rect1.x < rect2.x + rect2.width &&
        rect1.x + rect1.width > rect2.x &&
        rect1.y < rect2.y + rect2.height &&
        rect1.height + rect1.y > rect2.y);
}

function simp_move(obj,b){
    if (obj.vsp > 0){
        for (var i = 0; i < obj.vsp*timefctr; i++){
            if (!COLL_RxR_Place(obj,b,0,1)){
                obj.y += 1;
            }else{
                obj.vsp = 0;
            }
        }
    } else if (obj.vsp < 0){
        for (var i = 0; i < -obj.vsp*timefctr; i++){
            if (!COLL_RxR_Place(obj,b,0,-1)){
                obj.y -= 1;
            }else{
                obj.vsp = 0;
            }
        }
    }

    if (obj.hsp > 0){
        for (var i = 0; i < obj.hsp*timefctr; i++){
            if (!COLL_RxR_Place(obj,b,1,0)){
                obj.x += 1;
            }else{
                obj.hsp = 0;
            }
        }
    } else if (obj.hsp < 0){
        for (var i = 0; i < -obj.hsp*timefctr; i++){
            if (!COLL_RxR_Place(obj,b,-1,0)){
                obj.x -= 1;
            }else{
                obj.hsp = 0;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// displayable PARENT / regular block
////////////////////////////////////////////////////////////////////////////////

function display(x, y, width, height) {
    this.image = null;
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    this.frame = 0;
    this.animCap = 0;
    this.depth = 0;
    this.bound = "rect";
}

display.prototype.draw = function (c) {
    c.fillStyle = "blue";
    c.fillRect(this.x,this.y,this.width,this.height);
}

display.prototype.update = function (b, keys) {
}

////////////////////////////////////////////////////////////////////////////////
// object PARENT
////////////////////////////////////////////////////////////////////////////////

function objt(x, y, width, height) {
    display.call(this, width, height, x, y);
    this.hsp = 0;
    this.vsp = 0;
}

objt.prototype.draw = function (c) {
    c.fillStyle = "red";
    c.fillRect(this.x,this.y,this.width,this.height);
}

objt.prototype.update = function (b, keys) {

    if (!COLL_RxR_Place(this,b,0,1)){
        this.vsp += grav;
    }else{
        this.vsp = 0;
        if (keys[0]){
            this.vsp -= 9;
        }
    }
    simp_move(this,b);
}

////////////////////////////////////////////////////////////////////////////////
// instance PARENT
////////////////////////////////////////////////////////////////////////////////

function instance(x, y, width, height) {
    objt.call(this, width, height, x, y);
    this.hp = 0;
}

instance.prototype.draw = function (c) {
    c.fillStyle = "green";
    c.fillRect(this.x,this.y,this.width,this.height);
}


////////////////////////////////////////////////////////////////////////////////
// blocks
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// polygon BLOCK
////////////////////////////////////////////////////////////////////////////////


function polyBlock(xx, yy) {
    objt.call(this, 0, 0, 1, 1);
    this.bound = "poly"
    this.xx = xx;
    this.yy = yy;
}

polyBlock.prototype.draw = function (c) {
    c.fillStyle = "purple";

    c.beginPath();
    c.moveTo(this.xx[0], this.yy[0]);

    for (var i = 1; i < this.xx.length; i++){
        c.lineTo(this.xx[i],this.yy[i]);
    }

    c.closePath();
    c.fill();
}

polyBlock.prototype.update = function (c) {
}
